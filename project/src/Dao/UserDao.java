package Dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Beans.UserDataBeans;
import base.DBManager;
public class UserDao {

	/**
	 * ユーザーIDを取得
	 *
	 * @param loginId
	 *            ログインID
	 * @param password
	 *            パスワード
	 * @return int ログインIDとパスワードが正しい場合対象のユーザーID 正しくない||登録されていない場合0
	 * @throws SQLException
	 *             呼び出し元にスロー
	 */
	public static int getUserId(String loginId, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM user WHERE login_id = ? ");
			st.setString(1, loginId);

			ResultSet rs = st.executeQuery();

			int userId = 0;
			while (rs.next()) {
				if (password.equals(rs.getString("login_password"))) {
					userId = rs.getInt("id");
					System.out.println("login succeeded");
					break;
				}
			}

			System.out.println("searching userId by loginId has been completed");
			return userId;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ユーザーIDからユーザー情報を取得する
	 *
	 * @param useId
	 *            ユーザーID
	 * @return udbList 引数から受け取った値に対応するデータを格納する
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためスロー
	 */
	public static UserDataBeans getUserDataBeansByUserId(int userId) throws SQLException {
		UserDataBeans udb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT id,name, login_id, login_password,birth_date,create_date,update_date FROM user WHERE id =" + userId);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setPassword(rs.getString("login_password"));
				udb.setBirthdate(rs.getDate("birth_date"));
				udb.setCreatedate(rs.getDate("create_date"));
				udb.setUpdatedate(rs.getDate("update_date"));
			}

			st.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("searching UserDataBeans by userId has been completed");
		return udb;
	}

	/**
	 * ユーザーIDからユーザー情報を取得する
	 *
	 * @param useId
	 *            ユーザーID
	 * @return udbList 引数から受け取った値に対応するデータを格納する
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためスロー
	 */

	//ゲットパラメーターの型がStringしかできなかったので、String userId用のユーザー取得Daoメソッド


	public static UserDataBeans getUserDataBeansByUserId(String userId) throws SQLException {
		UserDataBeans udb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT id,name, login_id, login_password,birth_date,create_date,update_date FROM user WHERE id =" + userId);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setLoginId(rs.getString("login_id"));
				udb.setPassword(rs.getString("login_password"));
				udb.setBirthdate(rs.getDate("birth_date"));
				udb.setCreatedate(rs.getDate("create_date"));
				udb.setUpdatedate(rs.getDate("update_date"));
			}

			st.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("searching UserDataBeans by userId has been completed");
		return udb;
	}

	/**
	 * ユーザー情報の更新処理を行う。
	 *
	 * @param user
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	public int UpdateUserInfo(String name,String password,String birthdate,String loginid,String id) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String  sql = "UPDATE user SET name=?,login_password=?,birth_date=? ,login_id=?,update_date=now() WHERE Id=?";

            Encryption en =new Encryption();
			String pass=en.encryption(password);
             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, name);
            pStmt.setString(2, pass);
            pStmt.setString(3, birthdate);
            pStmt.setString(4, loginid);
            pStmt.setString(5, id);
            int rs = pStmt.executeUpdate();

            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
            return 2;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 2;
                }
            }
        }
    }


	//新規ユーザー登録
	public void addUser(String loginId, String password, String user_name, String birth_date) {
    	Connection conn = null;
    		try {
    			// データベースへ接続
    			conn = DBManager.getConnection();
    			// INSERT文を準備
    			String sql = "INSERT INTO user (login_id,login_password,name,birth_date,create_date,update_date) VALUES (?,?,?,?,now(),now())";
    			Encryption en =new Encryption();
    			String pass=en.encryption(password);

    			PreparedStatement stmt = conn.prepareStatement(sql);
    			stmt.setString(1,loginId );
    			stmt.setString(2,pass );
    			stmt.setString(3,user_name);
    			stmt.setString(4,birth_date );

    			int result = stmt.executeUpdate();
    			// 追加された行数を出力
    			System.out.println(result);
    			stmt.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			} finally {
    				if (conn != null) {
    	                try {
    	                    conn.close();
    	                } catch (SQLException e) {
    	                    e.printStackTrace();
    	                }
    	            }

    			}

    }

	//登録済みログインIDを取得→サーブレットで新規登録ログインIDと比較チェック
	 public UserDataBeans loginidsearch(String loginid) {
	        Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE login_id = ?";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, loginid);

	            ResultSet rs = pStmt.executeQuery();


	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	            if (!rs.next()) {
	                return null;
	            }

	            // 必要なデータのみインスタンスのフィールドに追加
	            int Id = rs.getInt("id");
	            String loginId2 = rs.getString("login_id");
	            String name = rs.getString("name");
	            Date birthDate = rs.getDate("birth_date");
	            String password = rs.getString("password");
	            Date createDate = rs.getDate("create_date");
	            Date updateDate = rs.getDate("update_date");

	            UserDataBeans udb = new UserDataBeans(Id, loginId2,password, name, birthDate,  createDate, updateDate);
	            return  udb ;
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }


	//ユーザー情報全取得（管理者以外）
	public List<UserDataBeans> findAll() {
        Connection conn = null;
        List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user where id !=1";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("login_password");
                Date createDate = rs.getDate("create_date");
                Date updateDate = rs.getDate("update_date");
                UserDataBeans user = new UserDataBeans(id, loginId,password, name, birthDate,createDate,updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

	/**
	 * ユーザーIDからユーザー情報を取得する
	 *
	 * @param useId
	 *            ユーザーID
	 * @return udbList 引数から受け取った値に対応するデータを格納する
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためスロー
	 */
	public  List<UserDataBeans> getUserinfoBysearch(String name,String loginid,String birthdate) throws SQLException {
		 Connection con = null;
	        List<UserDataBeans> userList = new ArrayList<UserDataBeans>();

	        try {
	            // データベースへ接続
	            con = DBManager.getConnection();

	            // SELECT文を準備
	            // TODO: 未実装：管理者以外を取得するようSQLを変更する
	            String sql = "SELECT *  FROM user "
	            		+ "WHERE login_id = ? and name like ? and birth_date >=?";


	            PreparedStatement stmt = con.prepareStatement(sql);
				stmt.setString(1, loginid);
				stmt.setString(2, '%' + name + '%');
				stmt.setString(3, birthdate);
	             // SELECTを実行し、結果表を取得
	            ResultSet rs = stmt.executeQuery();

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String loginId = rs.getString("login_id");
	                String Name = rs.getString("name");
	                Date birthDate = rs.getDate("birth_date");
	                String password = rs.getString("login_password");
	                Date createDate = rs.getDate("create_date");
	                Date updateDate = rs.getDate("update_date");
	                UserDataBeans user = new UserDataBeans(id, loginId,password, Name, birthDate,createDate,updateDate);

	                userList.add(user);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (con != null) {
	                try {
	                    con.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return userList;
	}


}