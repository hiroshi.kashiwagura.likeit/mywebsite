package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Beans.BuyDataBeans;
import base.DBManager;

public class BuyDao {
	/**
	 * 購入情報登録処理
	 * @param bdb 購入情報
	 * @throws SQLException 呼び出し元にスローさせるため
	 */
	public void InsertBuyItem (BuyDataBeans bdb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO buy (user_id,item_id,item_price,item_name,category_id,buy_date,img_url) values (?,?,?,?,?,current_date(),?)");
			st.setInt(1, bdb.getUserId());
			st.setInt(2, bdb.getItemId());
			st.setInt(3, bdb.getItemPrice());
			st.setString(4, bdb.getItemName());
			st.setInt(5, bdb.getCategoryId());
			st.setString(6, bdb.getImgURL());
			st.executeUpdate();
			System.out.println("inserting buy-datas has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入IDによる購入情報検索
	 * @param buyId
	 * @return BuyDataBeans
	 * 				購入情報のデータを持つJavaBeansのリスト
	 * @throws SQLException
	 * 				呼び出し元にスローさせるため
	 */
	public List<BuyDataBeans> getBuyHistoryByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT * FROM buy where user_id = ? ");
			st.setInt(1, buyId);
			ResultSet rs = st.executeQuery();
			List<BuyDataBeans> BDB = new ArrayList<BuyDataBeans>();
			while(rs.next()) {
				BuyDataBeans bdb = new BuyDataBeans();
				bdb.setId(rs.getInt("id"));
				bdb.setUserId(rs.getInt("user_id"));
				bdb.setItemId(rs.getInt("item_id"));
				bdb.setItemPrice(rs.getInt("item_price"));
				bdb.setItemName(rs.getString("item_name"));
				bdb.setCategoryId(rs.getInt("category_id"));
				bdb.setBuyDate(rs.getDate("buy_date"));
				bdb.setImgURL(rs.getString("img_url"));

				BDB.add(bdb);
			}
			System.out.println("searching BuyDataBeans by buyID has been completed");
			return BDB;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//人気商品情報を全取得
			public List<BuyDataBeans> getPopularItem() {
		        Connection conn = null;

				List<BuyDataBeans> popularitemList = new ArrayList<BuyDataBeans>();
		        try {
		            // データベースへ接続
		            conn = DBManager.getConnection();

		            // SELECT文を準備
		            // TODO: 未実装：管理者以外を取得するようSQLを変更する
		            String sql = "SELECT id,user_id,item_id,item_price,item_name,category_id,buy_date,img_url,count(id)as popular FROM buy GROUP BY item_id ORDER BY popular DESC";

		             // SELECTを実行し、結果表を取得
		            Statement stmt = conn.createStatement();
		            ResultSet rs = stmt.executeQuery(sql);



		            // 結果表に格納されたレコードの内容を
		            // Userインスタンスに設定し、ArrayListインスタンスに追加
		            while (rs.next()) {
		            	BuyDataBeans bdb = new BuyDataBeans();
		            	bdb.setId(rs.getInt("id"));
						bdb.setUserId(rs.getInt("user_id"));
						bdb.setItemId(rs.getInt("item_id"));
						bdb.setItemPrice(rs.getInt("item_price"));
						bdb.setItemName(rs.getString("item_name"));
						bdb.setCategoryId(rs.getInt("category_id"));
						bdb.setBuyDate(rs.getDate("buy_date"));
						bdb.setImgURL(rs.getString("img_url"));

						popularitemList.add(bdb);


		            }
		        } catch (SQLException e) {
		            e.printStackTrace();
		            return null;
		        } finally {
		            // データベース切断
		            if (conn != null) {
		                try {
		                    conn.close();
		                } catch (SQLException e) {
		                    e.printStackTrace();
		                    return null;
		                }
		            }
		        }
		        return popularitemList;
		    }
}
