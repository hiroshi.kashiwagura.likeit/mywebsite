package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Beans.ItemDataBeans;
import base.DBManager;

public class ItemDao {


	//商品情報を全取得
		public List<ItemDataBeans> findAll() {
	        Connection conn = null;
	        List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            // TODO: 未実装：管理者以外を取得するようSQLを変更する
	            String sql = "SELECT * FROM item";

	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	            	ItemDataBeans item = new ItemDataBeans();
	                item.setId(rs.getInt("id"));
	                item.setPrice(rs.getInt("money"));
	                item.setName(rs.getString("name"));
	                item.setCategoryid(rs.getInt("categoryid"));
	                item.setCount(rs.getInt("count"));
	                item.setDetail(rs.getString("detail"));
	                item.setUpdatedate(rs.getDate("update_date"));
	                item.setCreatedate(rs.getDate("create_date"));
	                item.setImgURL(rs.getString("img_url"));


	                itemList.add(item);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return itemList;
	    }
		//商品検索（部分一致検索）
		public  List<ItemDataBeans> findProductBySearch(String name) throws SQLException {
			 Connection con = null;
		        List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		        try {
		            // データベースへ接続
		            con = DBManager.getConnection();

		            // SELECT文を準備
		            // TODO: 未実装：管理者以外を取得するようSQLを変更する
		            String sql = "SELECT *  FROM item where name like ? ";


		            PreparedStatement stmt = con.prepareStatement(sql);
					stmt.setString(1, '%' + name + '%');
		             // SELECTを実行し、結果表を取得
		            ResultSet rs = stmt.executeQuery();

		            // 結果表に格納されたレコードの内容を
		            // Userインスタンスに設定し、ArrayListインスタンスに追加
		            while (rs.next()) {
		            	ItemDataBeans item = new ItemDataBeans();
		                item.setId(rs.getInt("id"));
		                item.setPrice(rs.getInt("money"));
		                item.setName(rs.getString("name"));
		                item.setCategoryid(rs.getInt("categoryid"));
		                item.setCount(rs.getInt("count"));
		                item.setDetail(rs.getString("detail"));
		                item.setUpdatedate(rs.getDate("update_date"));
		                item.setCreatedate(rs.getDate("create_date"));
		                item.setImgURL(rs.getString("img_url"));


		                itemList.add(item);
		            }
		        } catch (SQLException e) {
		            e.printStackTrace();
		            return null;
		        } finally {
		            // データベース切断
		            if (con != null) {
		                try {
		                    con.close();
		                } catch (SQLException e) {
		                    e.printStackTrace();
		                    return null;
		                }
		            }
		        }
		        return itemList;
		}

		//新規商品登録
		public void addItem(String name, String price, String categoryId, String createdate,String detail,String count,String imgName) {
	    	Connection conn = null;
	    		try {
	    			// データベースへ接続
	    			conn = DBManager.getConnection();
	    			// INSERT文を準備
	    			String sql = "INSERT INTO item (money,name,categoryId,count,detail,create_date,img_url) VALUES (?,?,?,?,?,?,?)";


	    			PreparedStatement stmt = conn.prepareStatement(sql);
	    			stmt.setString(1,price );
	    			stmt.setString(2,name);
	    			stmt.setString(3,categoryId);
	    			stmt.setString(4,count);
	    			stmt.setString(5,detail);
	    			stmt.setString(6,createdate);
	    			stmt.setString(7,imgName);

	    			int result = stmt.executeUpdate();
	    			// 追加された行数を出力
	    			System.out.println(result);
	    			stmt.close();
	    			} catch (SQLException e) {
	    				e.printStackTrace();
	    			} finally {
	    				if (conn != null) {
	    	                try {
	    	                    conn.close();
	    	                } catch (SQLException e) {
	    	                    e.printStackTrace();
	    	                }
	    	            }

	    			}

	    }

		//管理者用の商品情報を全取得
				public List<ItemDataBeans> MasterFindAllItem() {
			        Connection conn = null;
			        List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			        try {
			            // データベースへ接続
			            conn = DBManager.getConnection();

			            // SELECT文を準備
			            // TODO: 未実装：管理者以外を取得するようSQLを変更する
			            String sql = "SELECT * FROM item";

			             // SELECTを実行し、結果表を取得
			            Statement stmt = conn.createStatement();
			            ResultSet rs = stmt.executeQuery(sql);

			            // 結果表に格納されたレコードの内容を
			            // Userインスタンスに設定し、ArrayListインスタンスに追加
			            while (rs.next()) {
			            	ItemDataBeans item = new ItemDataBeans();
			                item.setId(rs.getInt("id"));
			                item.setPrice(rs.getInt("money"));
			                item.setName(rs.getString("name"));
			                item.setCategoryid(rs.getInt("categoryid"));
			                item.setCount(rs.getInt("count"));
			                item.setDetail(rs.getString("detail"));
			                item.setUpdatedate(rs.getDate("update_date"));
			                item.setCreatedate(rs.getDate("create_date"));
			                item.setImgURL(rs.getString("img_url"));
			                item.setCount(rs.getInt("count"));


			                itemList.add(item);
			            }
			        } catch (SQLException e) {
			            e.printStackTrace();
			            return null;
			        } finally {
			            // データベース切断
			            if (conn != null) {
			                try {
			                    conn.close();
			                } catch (SQLException e) {
			                    e.printStackTrace();
			                    return null;
			                }
			            }
			        }
			        return itemList;
			    }


				//商品IDを基に商品情報を取得
				public  ItemDataBeans getItemByItemID(String itemId) throws SQLException {
					Connection con = null;
					PreparedStatement st = null;
					try {
						con = DBManager.getConnection();

						st = con.prepareStatement("SELECT * FROM item WHERE id = ?");
						st.setString(1, itemId);

						ResultSet rs = st.executeQuery();

						ItemDataBeans item = new ItemDataBeans();
						if (rs.next()) {
							item.setId(rs.getInt("id"));
			                item.setPrice(rs.getInt("money"));
			                item.setName(rs.getString("name"));
			                item.setDetail(rs.getString("detail"));
			                item.setImgURL(rs.getString("img_url"));
			                item.setCategoryid(rs.getInt("categoryid"));
						}

						System.out.println("searching item by itemID has been completed");

						return item;
					} catch (SQLException e) {
						System.out.println(e.getMessage());
						throw new SQLException(e);
					} finally {
						if (con != null) {
							con.close();
						}
					}
				}
			//商品IDを基に商品を削除
				 public int ItemDelete(String id) {
				        Connection conn = null;
				        try {
				            // データベースへ接続
				            conn = DBManager.getConnection();

				            // SELECT文を準備
				            String  sql = "delete from item  WHERE Id=?";

				            PreparedStatement pStmt = conn.prepareStatement(sql);
				            pStmt.setString(1, id);
				            int rs = pStmt.executeUpdate();

				            return rs;


				        } catch (SQLException e) {
				            e.printStackTrace();
				            return 2;
				        } finally {
				            // データベース切断
				            if (conn != null) {
				                try {
				                    conn.close();
				                } catch (SQLException e) {
				                    e.printStackTrace();
				                    return 2;
				                }
				            }
				        }
				    }


				 //商品情報（個数）の更新
				 public  void updateItemCount(String item_id) throws SQLException {

						Connection con = null;
						PreparedStatement st = null;

						try {

							con = DBManager.getConnection();
							st = con.prepareStatement("UPDATE item SET count=count-1 where id = ?;");
							st.setString(1,item_id);
							st.executeUpdate();
							System.out.println("update has been completed");

							st.close();
							System.out.println("searching updated-UserDataBeans has been completed");

						} catch (SQLException e) {
							System.out.println(e.getMessage());
							throw new SQLException(e);
						} finally {
							if (con != null) {
								con.close();
							}
						}
					}
				 /**
					 * ランダムで引数指定分のItemDataBeansを取得
					 * @param limit 取得したいかず
					 * @return <ItemDataBeans>
					 * @throws SQLException
					 */
					public static ArrayList<ItemDataBeans> getRandItem(int limit) throws SQLException {
						Connection con = null;
						PreparedStatement st = null;
						try {
							con = DBManager.getConnection();

							st = con.prepareStatement("SELECT * FROM item ORDER BY RAND() LIMIT ? ");
							st.setInt(1, limit);

							ResultSet rs = st.executeQuery();

							ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

							while (rs.next()) {
								ItemDataBeans item = new ItemDataBeans();
								item.setId(rs.getInt("id"));
				                item.setPrice(rs.getInt("money"));
				                item.setName(rs.getString("name"));
				                item.setCategoryid(rs.getInt("categoryid"));
				                item.setCount(rs.getInt("count"));
				                item.setDetail(rs.getString("detail"));
				                item.setUpdatedate(rs.getDate("update_date"));
				                item.setCreatedate(rs.getDate("create_date"));
				                item.setImgURL(rs.getString("img_url"));
								itemList.add(item);
							}
							System.out.println("getAllItem completed");
							return itemList;
						} catch (SQLException e) {
							System.out.println(e.getMessage());
							throw new SQLException(e);
						} finally {
							if (con != null) {
								con.close();
							}
						}
					}

}
