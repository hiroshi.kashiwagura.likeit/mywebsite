package Dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Beans.ItemDataBeans;
import Beans.ItemDataBeansByCategory;
import base.DBManager;

public class ItemByCategoryDao {

		//商品情報を全取得
				public List<ItemDataBeans> findAll(String id) {
			        Connection conn = null;
			        List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			        try {
			            // データベースへ接続
			            conn = DBManager.getConnection();

			            // SELECT文を準備
			            // TODO: 未実装：管理者以外を取得するようSQLを変更する
			            String sql = "SELECT * FROM item join item_category on item.categoryId = item_category.id where item.categoryId = ?";

			             // SELECTを実行し、結果表を取得
			            PreparedStatement pStmt = conn.prepareStatement(sql);
			            pStmt.setString(1, id);

			            ResultSet rs = pStmt.executeQuery();

			            // 結果表に格納されたレコードの内容を
			            // Userインスタンスに設定し、ArrayListインスタンスに追加
			            while (rs.next()) {
			            	ItemDataBeans item = new ItemDataBeans();
			                item.setId(rs.getInt("id"));
			                item.setPrice(rs.getInt("money"));
			                item.setName(rs.getString("name"));
			                item.setCategoryid(rs.getInt("categoryid"));
			                item.setCount(rs.getInt("count"));
			                item.setDetail(rs.getString("detail"));
			                item.setUpdatedate(rs.getDate("update_date"));
			                item.setCreatedate(rs.getDate("create_date"));
			                item.setImgURL(rs.getString("img_url"));


			                itemList.add(item);
			            }
			        } catch (SQLException e) {
			            e.printStackTrace();
			            return null;
			        } finally {
			            // データベース切断
			            if (conn != null) {
			                try {
			                    conn.close();
			                } catch (SQLException e) {
			                    e.printStackTrace();
			                    return null;
			                }
			            }
			        }
			        return itemList;
			    }


				//カテゴリーの名前を全取得（トップページのジャンル別ページ出力用）
				public List<ItemDataBeansByCategory> findAll() {
			        Connection conn = null;
			        List<ItemDataBeansByCategory> itemList = new ArrayList<ItemDataBeansByCategory>();

			        try {
			            // データベースへ接続
			            conn = DBManager.getConnection();

			            // SELECT文を準備
			            // TODO: 未実装：管理者以外を取得するようSQLを変更する
			            String sql = "SELECT * FROM item_category ";
			             // SELECTを実行し、結果表を取得
			            Statement stmt = conn.createStatement();
			            ResultSet rs = stmt.executeQuery(sql);

			            // 結果表に格納されたレコードの内容を
			            // Userインスタンスに設定し、ArrayListインスタンスに追加
			            while (rs.next()) {
			            	ItemDataBeansByCategory item = new ItemDataBeansByCategory();
			            	item.setId(rs.getInt("id"));
			                item.setCategoryName(rs.getString("category_name"));


			                itemList.add(item);
			            }
			        } catch (SQLException e) {
			            e.printStackTrace();
			            return null;
			        } finally {
			            // データベース切断
			            if (conn != null) {
			                try {
			                    conn.close();
			                } catch (SQLException e) {
			                    e.printStackTrace();
			                    return null;
			                }
			            }
			        }
			        return itemList;
			    }
}
