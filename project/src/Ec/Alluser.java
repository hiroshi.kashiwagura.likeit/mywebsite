package Ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.UserDataBeans;
import Dao.UserDao;

/**
 * Servlet implementation class Alluser
 */
@WebServlet("/Alluser")
public class Alluser extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Alluser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //全ユーザー情報を取得してセット
        UserDao UserDao = new UserDao();
        List<UserDataBeans> userList = UserDao.findAll();

        request.setAttribute("user", userList);

        //セッション開始
        //ユーザーのログイン情報をゲット
        HttpSession session = request.getSession();
        int userId = (int)session.getAttribute("userId");

        request.setAttribute("id", userId);

     // フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/alluser.jsp");
		dispatcher.forward(request, response);
	}


}
