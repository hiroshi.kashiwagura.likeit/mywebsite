package Ec;


import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.Encryption;
import Dao.UserDao;
/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
				dispatcher.forward(request, response);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //セッション開始
        //ユーザーのログイン情報をセット
        HttpSession session = request.getSession();
        //UserDaoに接続
        try {
        	  // リクエストパラメータの取得
            String Loginid = request.getParameter("Loginid");
            String Password = request.getParameter("Password");

            //パスワードを暗号化
            Encryption en =new Encryption();
			String pass=en.encryption(Password);

			int userId=UserDao.getUserId(Loginid, pass);
			if(userId!=0) {
				session.setAttribute("userId", userId);
				//トップページにリダイレクト
				response.sendRedirect("Home");
			}else {

				request.setAttribute("errMsg","入力内容が正しくありません");
				// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
				dispatcher.forward(request, response);


			}
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			//ログインページにリダイレクト
			response.sendRedirect("Login");
		}
	}


}
