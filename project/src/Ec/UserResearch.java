package Ec;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.UserDataBeans;
import Dao.UserDao;

/**
 * Servlet implementation class UserResearch
 */
@WebServlet("/UserResearch")
public class UserResearch extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserResearch() {
        super();
        // TODO Auto-generated constructor stub
    }



    /**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        String loginid = request.getParameter("loginid");
        String name = request.getParameter("name");
        String birthdate = request.getParameter("birthdate");

        if((name.equals("")) || ( loginid.equals("")) || (birthdate.equals(""))){
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			// ユーザー登録画面にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/alluser.jsp");
			dispatcher.forward(request, response);
			return;
		}

        	try {
        		 //条件に合ったユーザー情報を取得してセット
                UserDao UserDao = new UserDao();
                List<UserDataBeans> userList;
        		userList = UserDao.getUserinfoBysearch(loginid,name,birthdate);
        		request.setAttribute("user", userList);
        	} catch (SQLException e) {
        		// TODO 自動生成された catch ブロック
        		e.printStackTrace();
        	}



        // フォワード
   		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/alluser.jsp");
   		dispatcher.forward(request, response);
	}


}
