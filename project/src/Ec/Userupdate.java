package Ec;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.UserDataBeans;
import Dao.UserDao;

/**
 * Servlet implementation class Userupdate
 */
@WebServlet("/Userupdate")
public class Userupdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Userupdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //セッション開始
        //ユーザーのログイン情報をゲット
        HttpSession session = request.getSession();
        int userId = (int)session.getAttribute("userId");

        //getしたユーザーIDを基にユーザー情報を取得
        try {
			UserDataBeans userinfo =UserDao.getUserDataBeansByUserId(userId);

			request.setAttribute("userinfo", userinfo);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

        // フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        String loginid =request.getParameter("loginid");
        String password =request.getParameter("password");
        String password1 =request.getParameter("password1");
        String name =request.getParameter("name");
        String birthdate =request.getParameter("birthdate");
        String id =request.getParameter("id");

        if(!(password.equals(password1))){
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if((loginid.equals("")) || ( password.equals("")) || (password1.equals("")) || (name.equals("")) || (birthdate.equals(""))){
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}


		UserDao userDao = new UserDao();
		userDao.UpdateUserInfo(name,password,birthdate,loginid,id);


		// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/home.jsp");
				dispatcher.forward(request, response);
	}

}
