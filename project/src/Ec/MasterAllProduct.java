package Ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.ItemDataBeans;
import Dao.ItemDao;

/**
 * Servlet implementation class MasterAllProduct
 */
@WebServlet("/MasterAllProduct")
public class MasterAllProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MasterAllProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //全商品情報を取得してセット
        ItemDao ItemDao = new ItemDao();
        List<ItemDataBeans> itemList = ItemDao.MasterFindAllItem();

        request.setAttribute("item", itemList);

        // フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/masterallproduct.jsp");
				dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
