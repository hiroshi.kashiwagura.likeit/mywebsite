package Ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import Dao.ItemDao;

/**
 * Servlet implementation class MasterProductRes
 */
@WebServlet("/MasterProductRes")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-1P6BTJU.000\\Documents\\mywebsite\\project\\WebContent\\image",  maxFileSize = 1048576)
public class MasterProductRes extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MasterProductRes() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/masterproductres.jsp");
				dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        String name=request.getParameter("name");
        String price=request.getParameter("price");
        String categoryId=request.getParameter("categoryId");
        String createdate=request.getParameter("createdate");
        String detail=request.getParameter("detail");
        String count=request.getParameter("count");
        Part image=request.getPart("image");
        String imgName = this.getFileName(image);
        image.write(imgName);

        if((name.equals("")) || ( price.equals("")) || (categoryId.equals("")) || (createdate.equals(""))
        		|| (detail.equals("")) || (count.equals(""))){
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			// ユーザー登録画面にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/masterproductres.jsp");
			dispatcher.forward(request, response);
			return;
		}

        ItemDao ItemDao = new ItemDao();
        ItemDao.addItem(name,price,categoryId,createdate,detail,count,imgName);

        //トップページにリダイレクト
		response.sendRedirect("Home");
	}

	 private String getFileName(Part part) {
	        String name = null;
	        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
	            if (dispotion.trim().startsWith("filename")) {
	                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
	                name = name.substring(name.lastIndexOf("\\") + 1);
	                break;
	            }
	        }
	        return name;
	    }

}
