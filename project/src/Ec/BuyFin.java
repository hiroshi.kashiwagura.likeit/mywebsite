package Ec;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.BuyDataBeans;
import Beans.ItemDataBeans;
import Dao.BuyDao;
import Dao.ItemDao;

/**
 * Servlet implementation class BuyFin
 */
@WebServlet("/BuyFin")
public class BuyFin extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyFin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        String item_id=request.getParameter("item_id");


		try {
			 ItemDao ItemDao =new ItemDao();
		        ItemDataBeans item;
			item = ItemDao.getItemByItemID(item_id);
			request.setAttribute("item", item);

			 //セッション開始
	        //ユーザーのログイン情報をゲット
	        HttpSession session = request.getSession();
	        int userId = (int)session.getAttribute("userId");

	        //購入情報登録処理
	        BuyDataBeans bdb =new BuyDataBeans();
	        bdb.setUserId(userId);
			bdb.setItemId(item.getId());
			bdb.setItemPrice(item.getPrice());
			bdb.setItemName(item.getName());
			bdb.setCategoryId(item.getCategoryid());
			bdb.setImgURL(item.getImgURL());

			BuyDao BuyDao =new BuyDao();
			BuyDao.InsertBuyItem(bdb);

			//商品の個数を一つ減らす
			ItemDao.updateItemCount(item_id);

			//カートから購入した情報を消去
			ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

			//for文用にitem_idをキャスト
			int itemid =Integer.parseInt(item_id);

			for(int i=0;i<cart.size();i++) {
				if(itemid == cart.get(i).getId()) {
					// indexOfメソッドでindex番号を取得
			        cart.remove(cart.get(i));
			        break;
				}



			}

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


        request.getRequestDispatcher("/WEB-INF/jsp/buyfin.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
