package Ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.UserDataBeans;
import Dao.UserDao;

/**
 * Servlet implementation class Userregistration
 */
@WebServlet("/Userregistration")
public class Userregistration extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Userregistration() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
     // フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userregistration.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //入力した文字を取得
        String loginid =request.getParameter("loginid");
        String password =request.getParameter("password");
        String password1 =request.getParameter("password1");
        String name =request.getParameter("name");
        String birthdate =request.getParameter("birthdate");

        if(!(password.equals(password1))){
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			// ユーザー登録画面にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if((loginid.equals("")) || ( password.equals("")) || (password1.equals("")) || (name.equals("")) || (birthdate.equals(""))){
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			// ユーザー登録画面にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//登録済みログインIDを使っていないかチェック
		UserDao UserDao = new UserDao();
		UserDataBeans user = UserDao.loginidsearch(loginid);

		if(user != null) {
				request.setAttribute("errMsg", "入力された内容は正しくありません。");



			// ユーザー登録画面にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);

		}

		  //データベースに新規情報を登録
        UserDao userDao = new UserDao();
		userDao.addUser(loginid,password,name,birthdate);

		request.setAttribute("Msg", "ユーザー登録完了。ログインしてください");

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);


	}

}
