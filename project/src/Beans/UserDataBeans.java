package Beans;

import java.io.Serializable;
import java.util.Date;

/**
 * ユーザー
 * @author d-yamaguchi
 *
 */
public class UserDataBeans implements Serializable {
	private String name;
	private String loginId;
	private String password;
	private int id;
	private Date birthdate;
	private Date createdate;
	private Date updatedate;

	// コンストラクタ
	public UserDataBeans(int id, String loginId, String password, String name, Date birthDate, Date createDate,
			Date updateDate) {
		this.name = name;
		this.birthdate = birthDate;
		this.loginId = loginId;
		this.createdate = createDate;
		this.id = id;
		this.updatedate = updateDate;
	}

	// コンストラクタ
	public UserDataBeans() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public Date getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public void UserLoginInfo(String loginId, String password) {
		this.loginId = loginId;
		this.password = password;

	}
}
