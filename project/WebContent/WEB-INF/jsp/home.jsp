<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ホーム画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   <link rel="stylesheet" href="css/homemenu.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/cssstyle.css">
</head>
<body>
    <div class="header-area">

        <header class="page-header">
			<br> <br>
            <h1 class="header center">Amazooon</h1>
                <div class="bottonstyle">
                    <a class="btn btn-outline-primary" href="Userinfo" >ユーザー情報 </a>
                    <a class="btn btn-outline-primary" href="Cart">カート </a>
                    <a class="btn btn-outline-primary" href="Logout">ログアウト </a>
                    <a class="btn btn-outline-primary" href="Alluser">ユーザー一覧 </a>
                    <a class="btn btn-outline-primary" href="Home">トップページへ</a>
                </div>
        </header>
    </div>
    <br><br>
                 <div class="search">
                <div class="row center">
	             <div class="input-field col s8 offset-s2">
		                <form action="Productsearch" method="get">
                             <h2>商品検索</h2>
            						<p>商品名</p>
            							<input type="text" name="name" style="width;200px," class="btn page-link text-dark d-inline-block">
			              				<button class="btn btn-outline-primary" type="submit" name="action">検索</button>
		                </form>
	             </div>
                </div>
                </div>
			<br> <br>


     <div class="row">
    <aside>
        <div class="menu col-3">
          <ul>
              <li><a class="btn btn-outline-primary" href="Allproduct">商品一覧 </a></li>
             <c:forEach var="ItemDataBeansByCategory" items="${ItemCategoryName}" >
                	  <li><a class="btn btn-outline-primary" href="CategoryProduct?id=${ItemDataBeansByCategory.id}">${ItemDataBeansByCategory.categoryName} </a></li>
             </c:forEach>
          </ul>
        </div>
    </aside>

            <div class="col-9" >
                <div class="text-center">
            <h2>人気商品</h2>
            <div class="productstyle">
            	<c:forEach var="BuyDataBeans" items="${popularitemList}" >
               <div class="productstyle">
                <img src="image/${BuyDataBeans.imgURL}" alt="商品画像">
                    <ul>
                        <li>${BuyDataBeans.itemName}</li>
                        <li>${BuyDataBeans.itemPrice}</li>
                        <li><form action="ItemAdd" method="post" >
                        		<input type="hidden" name="item_id"  value="${BuyDataBeans.itemId}">
								<button class="btn btn-outline-primary" type="submit" name="action">買い物かごに追加</button>
							</form></li>
                        <li><a href="ProductDetail?item_id=${BuyDataBeans.itemId}" class="btn btn-outline-primary">詳細</a></li>
                    </ul>
                 </div>
                 </c:forEach>
				</div>

            </div>
            </div>
    </div>
</body>
</html>