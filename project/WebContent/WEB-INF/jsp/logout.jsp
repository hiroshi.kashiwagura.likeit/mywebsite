<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログアウト</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   <link rel="stylesheet" href="css/homemenu.css">
    <link rel="stylesheet" href="css/header.css">
</head>
<body>
 <div class="header-area">

        <header class="page-header">
			<br> <br>
            <h1 class="header center">Amazooon</h1>
                <div class="bottonstyle">
                    <a class="btn btn-outline-primary" href="Userinfo" >ユーザー情報 </a>
                    <a class="btn btn-outline-primary" href="Cart">カート </a>
                    <a class="btn btn-outline-primary" href="Logout">ログアウト </a>
                    <a class="btn btn-outline-primary" href="Alluser">ユーザー一覧 </a>
                    <a class="btn btn-outline-primary" href="Home">トップページへ</a>
                </div>
        </header>
    </div>

    <br><br>
    <div class="text-center">
        <h2>ログアウト画面</h2>
    </div>
    <div class="text-center">
        <p>本当にログアウトしますか？</p>
        <button class="btn btn-outline-primary" type="submit" name="action">ログアウト </button>
    </div>
</body>
</html>