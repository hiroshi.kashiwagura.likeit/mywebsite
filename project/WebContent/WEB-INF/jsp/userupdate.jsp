<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報変更</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   <link rel="stylesheet" href="css/homemenu.css">
    <link rel="stylesheet" href="css/header.css">
</head>
<body>
  <div class="header-area">

        <header class="page-header">
			<br> <br>
            <h1 class="header center">Amazooon</h1>
                <div class="bottonstyle">
                    <a class="btn btn-outline-primary" href="Userinfo" >ユーザー情報 </a>
                    <a class="btn btn-outline-primary" href="Cart">カート </a>
                    <a class="btn btn-outline-primary" href="Logout">ログアウト </a>
                    <a class="btn btn-outline-primary" href="Alluser">ユーザー一覧 </a>
                    <a class="btn btn-outline-primary" href="Home">トップページへ</a>
                </div>
        </header>
  </div>
  <div class="text-center">
  	<h1>ユーザー情報変更</h1>
    	<div>
        	<form action="Userupdate" method="post">
            	<p>ログインID<input type="text" name="loginid" value="${userinfo.loginId}" ></p>
                <p>パスワード<input type="password" name="password"></p>
                <P>パスワード（確認）<input type="password" name="password1"></P>
                <p>ユーザー名<input type="text" name="name" value="${userinfo.name}"></p>
                <p>生年月日<input type="date" name="birthdate" value="${userinfo.birthdate}"></p>
				<input type="hidden" name="id" value="${userinfo.id}">
                    <button class="btn btn-outline-primary" type="submit">ユーザー情報変更 </button></form>
        </div>
   </div>
</body>
</html>