<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品一覧</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   <link rel="stylesheet" href="css/homemenu.css">
    <link rel="stylesheet" href="css/header.css">
</head>
<body>
      <div class="header-area">

        <header class="page-header">
			<br> <br>
            <h1 class="header center">Amazooon</h1>
                <div class="bottonstyle">
                    <a class="btn btn-outline-primary" href="Userinfo" >ユーザー情報 </a>
                    <a class="btn btn-outline-primary" href="Cart">カート </a>
                    <a class="btn btn-outline-primary" href="Logout">ログアウト </a>
                    <a class="btn btn-outline-primary" href="Alluser">ユーザー一覧 </a>
                    <a class="btn btn-outline-primary" href="Home">トップページへ</a>
                </div>
        </header>
    </div>

        <div class="text-center">
            <h2>ユーザー一覧</h2>
             <div class="search">
                <div class="row center">
	             <div class="input-field col s8 offset-s2">
                             <h2>ユーザー検索</h2>
                         <form action="UserResearch" method="get">
            						<p>ログインID</p>
            							<input type="text" name="loginid" style="width;200px," class="btn page-link text-dark d-inline-block">
            						<p>ユーザー名</p>
            							<input type="text" name="name" style="width;200px," class="btn page-link text-dark d-inline-block">
        							<p>生年月日</p>
            							<input type="date" placeholder="生年月日" name="birthdate">

			              <p><button class="btn btn-outline-primary" type="submit" name="action">検索</button></p>
		                </form>
		                <p>${errMsg}</p>
	             </div>
                </div>
                </div>

            <div class="container">
            <table>
              <tr>
                <th>ログインID</th>
                <th>ユーザー名</th>
                <th>生年月日</th>
                <th></th>
              </tr>
               <c:forEach var="UserDataBeans" items="${user}" >
              	<tr>
                 	<td>${UserDataBeans.loginId}</td>
                  	<td>${UserDataBeans.name}</td>
                  	<td>${UserDataBeans.birthdate}</td>
                  	<td> <form  action="Userinfo?id=${UserDataBeans.id}" method="post">
                    	<button class="btn btn-outline-primary" type="submit" name="action">詳細 </button></form></td>
              	</tr>
               </c:forEach>
            </table>
            </div>
    </div>
</body>
</html>