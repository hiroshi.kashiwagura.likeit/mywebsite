<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>master商品一覧</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   <link rel="stylesheet" href="css/homemenu.css">
    <link rel="stylesheet" href="css/header.css">
</head>
<body>
  <div class="header-area">

        <header class="page-header">
			<br> <br>
            <h1 class="header center">Amazooon</h1>
                <div class="bottonstyle">
                    <a class="btn btn-outline-primary" href="Userinfo" >ユーザー情報 </a>
                    <a class="btn btn-outline-primary" href="Cart">カート </a>
                    <a class="btn btn-outline-primary" href="Logout">ログアウト </a>
                    <a class="btn btn-outline-primary" href="Alluser">ユーザー一覧 </a>
                    <a class="btn btn-outline-primary" href="Home">トップページへ</a>
                </div>
        </header>
    </div>

        <div class="text-center">
            <h2>master商品一覧</h2>
            <div>
            	<div class="container">
               	 <table border="1">
        	   	     <tr>
                         <th>金額</th>
        	   	         <th>商品名</th>
        	   	         <th>追加日時</th>
        	   	         <th>カテゴリーID</th>
                         <th>個数</th>
                         <th>詳細</th>
                         <th>画像</th>
                         <th></th>
                    </tr>
                        <c:forEach var="ItemDataBeans" items="${item}" >
        	   	          <tr>
                             <td>${ItemDataBeans.price}</td>
                             <td>${ItemDataBeans.name}</td>
                             <td>${ItemDataBeans.createdate}</td>
                             <td>${ItemDataBeans.categoryid}</td>
                             <td>${ItemDataBeans.count}</td>
                             <td>${ItemDataBeans.detail}</td>
                             <td><img src="image/${ItemDataBeans.imgURL}" alt="商品画像"></td>
							 <td> <form action="MasterProductDelete?item_id=${ItemDataBeans.id}" method="post">
            						<button class="btn btn-outline-danger" type="submit" name="action">削除</button>
		                		  </form></td>
                    	  </tr>
        	   	      </c:forEach>

                </table>
            </div>
    </div>
    </div>
</body>
</html>