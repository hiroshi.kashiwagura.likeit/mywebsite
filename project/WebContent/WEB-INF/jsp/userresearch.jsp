<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   <link rel="stylesheet" href="css/homemenu.css">
    <link rel="stylesheet" href="css/header.css">
</head>
<body>
  <div class="header-area">

        <header class="page-header">
			<br> <br>
            <h1 class="header center">Amazooon</h1>
                <div class="bottonstyle">
                    <a class="btn btn-outline-primary" href="Userinfo" >ユーザー情報 </a>
                    <a class="btn btn-outline-primary" href="Cart">カート </a>
                    <a class="btn btn-outline-primary" href="Logout">ログアウト </a>
                    <a class="btn btn-outline-primary" href="Alluser">ユーザー一覧 </a>
                    <a class="btn btn-outline-primary" href="Home">トップページへ</a>
                </div>
        </header>
    </div>

        <div class="text-center">
            <div class="userresearch">
                <h2>ユーザー検索</h2>
               <form class="form">
                   <p><input type="text" placeholder="ログインID"></p>
                   <p><input type="text" placeholder="ユーザー名"></p>
                   <p><input type="date" placeholder="生年月日">~<input type="date" placeholder="生年月日"></p>
                   <p><button type="submit" id="login-button">検索</button></p>
                </form>
                    <table border="1">
        	   	     <tr>
        	   	         <th>ログインID</th>
        	   	         <th>ユーザ名</th>
        	   	         <th>生年月日</th>
        	   	         <th></th>
                     </tr>
                        <c:forEach var="user" items="${userList}" >
        	   	          <tr>
                             <td>${user.loginId}</td>
                             <td>${user.name}</td>
                             <td>${user.birthDate}</td>
                             <td>
                               <c:if test="${userInfo.id ==1}" >
                                    <a href="UserDetailServlet?id=${user.id}" class="btn btn-primary" >詳細</a>
                            		<a href="UserUpdateServlet?id=${user.id}" class="btn btn-success" >更新</a>
                                    <a href="UserDeleteServlet?id=${user.id}" class="btn btn-danger" >削除</a></c:if>
                               <c:if test="${userInfo.id !=1}" >
                    			    <a href="UserDetailServlet?id=${user.id}" class="btn btn-primary" >詳細</a></c:if>
                               <c:if test="${userInfo.loginId == user.loginId}" >
                                    <a href="UserUpdateServlet?id=${user.id}" class="btn btn-success" >更新</a></c:if>

                             </td>
                    	  </tr>
        	   	      </c:forEach>
                    </table>
            </div>
    </div>
</body>
</html>