<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   <link rel="stylesheet" href="css/homemenu.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/cssstyle.css">
</head>
<body>
 <div class="header-area">

        <header class="page-header">
			<br> <br>
            <h1 class="header center">Amazooon</h1>
                <div class="bottonstyle">
                    <a class="btn btn-outline-primary" href="Userinfo" >ユーザー情報 </a>
                    <a class="btn btn-outline-primary" href="Cart">カート </a>
                    <a class="btn btn-outline-primary" href="Logout">ログアウト </a>
                    <a class="btn btn-outline-primary" href="Alluser">ユーザー一覧 </a>
                    <a class="btn btn-outline-primary" href="Home">トップページへ</a>
                </div>
        </header>
    </div>

        <div class="text-center">
            <h2>カート</h2>
            <br><br>
            <div>
            <div class="flex">
                <div class="productstyle">
                <c:forEach var="ItemDataBeans" items="${cart}" varStatus="status">
                <div class="productstyle">
                <img src="image/${ItemDataBeans.imgURL}" alt="商品画像">
                    <ul>
                        <li>${ItemDataBeans.name}</li>
                        <li>${ItemDataBeans.price}</li>
                        <li><a href="ProductDetail" class="btn btn-outline-primary">商品詳細</a></li>
                        <li><a href="Buy?item_id=${ItemDataBeans.id}" class="btn btn-outline-primary">購入</a></li>
                   </ul>
                   </div>
					</c:forEach>
                </div>

            </div>
                <br><br>
            </div>
            <p>${cartActionMessage}</p>
        </div>
</body>
</html>