<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   <link rel="stylesheet" href="css/homemenu.css">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/cssstyle.css">
</head>
	<body>
	<div class="header-area">

        <header class="page-header">
			<br> <br>
            <h1 class="header center">Amazooon</h1>
                <div class="bottonstyle">
                    <a class="btn btn-outline-primary" href="Userregistration">新規登録 </a>
                </div>
        </header>
    </div>
		 <form class="form-signin" action="Login" method="post" >
        <div class="col-4 mx-auto">
            <h1>ログイン画面</h1>
            <h2>ログインID</h2>
                <input type="text"  name="Loginid" class="btn btn-outline-primary" style="width;200px", >
            <h2>パスワード</h2>
                <input type="password" name="Password" style="width;200px," class="btn btn-outline-primary">
                <input type="submit"  value="ログイン" class="btn btn-outline-primary">
                <p class="red-text center-align">${errMsg}</p>
                <p class="red-text center-align">${Msg}</p>
        </div></form>


    </body>
</html>