<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   	<link rel="stylesheet" href="css/homemenu.css">
    <link rel="stylesheet" href="css/header.css">
</head>
<body>
 <div class="header-area">
        <header class="page-header">
			<br> <br>
            <h1 class="header center">Amazooon</h1>
                <div class="bottonstyle">
                    <a class="btn btn-outline-primary" href="Userinfo" >ユーザー情報 </a>
                    <a class="btn btn-outline-primary" href="Cart">カート </a>
                    <a class="btn btn-outline-primary" href="Logout">ログアウト </a>
                    <a class="btn btn-outline-primary" href="Alluser">ユーザー一覧 </a>
                    <a class="btn btn-outline-primary" href="Home">トップページへ</a>
                </div>
        </header>
    </div>
        <div class="text-center">
            <h1>ユーザー情報</h1>
            <div>
                <p>ログインID ${userinfo.loginId}</p>
                <p>ユーザー名 ${userinfo.name}</p>
                <p>生年月日 ${userinfo.birthdate}</p>
                <p>登録日時 ${userinfo.createdate}</p>
            </div>
            <div class="formaster">
            	<c:if test="${userinfo.id==1}" >
                     <a class="btn btn-outline-primary" href="MasterAllProduct">master商品一覧 </a>
                     <a class="btn btn-outline-primary" href="MasterProductRes">master商品登録</a></c:if>
            </div>
            <div>
                 <a class="btn btn-outline-primary" href="Userupdate">ユーザー情報更新 </a></div>
            <div>
                 <a class="btn btn-outline-primary" href="BuyHistory">ユーザー購入履歴 </a></div>
    </div>
</body>
</html>