<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規ユーザー登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   <link rel="stylesheet" href="css/homemenu.css">
    <link rel="stylesheet" href="css/header.css">
</head>
<body>
   <div class="header-area">

        <header class="page-header">
			<br> <br>
            <h1 class="header center">Amazooon</h1>
                <div class="bottonstyle">
                    <a class="btn btn-outline-primary" href="Userinfo" >ユーザー情報 </a>
                    <a class="btn btn-outline-primary" href="Cart">カート </a>
                    <a class="btn btn-outline-primary" href="Logout">ログアウト </a>
                    <a class="btn btn-outline-primary" href="Alluser">ユーザー一覧 </a>
                    <a class="btn btn-outline-primary" href="Home">トップページへ</a>
                </div>
        </header>
    </div>

        <div class="text-center">
            <h1>新規商品登録</h1>
            <form  action="MasterProductRes" method="post"enctype="multipart/form-data">
                <div>
                    <p>金額<input type="number" name="price" ></p>
                    <p>商品名<input type="text" name="name" ></p>
                        <p>タグID<select name="categoryId">
                            <option value="1">靴</option>
                            <option value="2">服</option>
                            <option value="3">ズボン</option>
                            <option value="4">帽子</option>
                            <option value="5">アウター</option>
                            <option value="6">下着</option>
                            </select></p>
                        <p>追加日時<input type="date" name="createdate" ></p>
                        <p>詳細<input type="text" name="detail" ></p>
                        <p>個数<input type="number" name="count"></p>
                        <p>画像<input type="file" name="image"></p>

                </div>
            		<button class="btn btn-outline-primary" type="submit">商品登録</button></form>
            	<div><p>${errMsg}</p></div>
    </div>
</body>
</html>