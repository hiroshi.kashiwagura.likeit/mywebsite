<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規ユーザー登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   <link rel="stylesheet" href="css/homemenu.css">
    <link rel="stylesheet" href="css/header.css">
</head>
<body>
 <div class="header-area">
 	<header class="page-header">
		<br> <br>
            <h1 class="header center">Amazooon</h1>

    </header>
  </div>
  	<div class="text-center">
    	<h1>新規ユーザー登録</h1>
            <form action="Userregistration" method="post">
            	<div>
                	<p>ログインID<input type="text" name="loginid" ></p>
                	<p>パスワード<input type="password" name="password" ></p>
                	<P>パスワード（確認）<input type="password" name="password1" ></P>
                	<p>ユーザー名<input type="text" name="name"></p>
                	<p>生年月日<input type="date" placeholder="生年月日" name="birthdate"></p>
            	</div>
                    <p><button class="btn btn-outline-primary" type="submit" name="action">登録 </button></p></form>
    </div>
</body>
</html>